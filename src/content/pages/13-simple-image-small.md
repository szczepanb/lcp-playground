---
#title: "How "
description: "How chrome browser calculates size of Text node elements."
prevUrl: "/pages/12-simple-image-hidden"
nextUrl: "/pages/14-two-images-one-bigger"
---
<div class="info-container">
    <div class="side-by-side">
        <div><img src="/1.jpg" width="100" height="100"/></div>
    </div>
    <button class="lets-check">Let's check</button>
</div>
<script>
let infoCont = document.querySelector('.info-container');
document.querySelector('.lets-check').addEventListener('click', function(){
    new PerformanceObserver((entryList) => {
        let lcp = entryList.getEntries()[entryList.getEntries().length-1];
        infoCont.insertAdjacentHTML('beforeend', `<p>width: ${lcp.element.clientWidth}, height: ${lcp.element.clientHeight}, size: ${lcp.element.clientWidth*lcp.element.clientHeight}</p>`);
        infoCont.insertAdjacentHTML('beforeend', `<p>LCP size: ${lcp.size}`);
        lcp.element.style.border = "2px solid red";
    }).observe({type: 'largest-contentful-paint', buffered: true});
});
</script>