---
#title: "How "
description: "How chrome browser calculates size of Text node elements."
prevUrl: "/pages/6b-example-text-lcp"
nextUrl: "/pages/8-two-another-text-change-bigger"
---

<div class="info-container">
    <div class="side-by-side">
        <span elementtiming="text-node-first">Your eyes can deceive you, don't trust them.</span>
        <span elementtiming="text-node-second">Wonderful girl. Either I’m going to kill her or I’m beginning to like her.</span>
    </div>
    <button class="lets-check">Let's check</button>
</div>
<style>
    .side-by-side {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .side-by-side > * {
        margin: 10px;
        text-align: center;
    }
</style>
<script>
let infoCont = document.querySelector('.info-container');
document.querySelector('.lets-check').addEventListener('click', function(){
    new PerformanceObserver((entryList) => {
        let lcp = entryList.getEntries()[entryList.getEntries().length-1];
        infoCont.insertAdjacentHTML('beforeend', `<p>width: ${lcp.element.clientWidth}, height: ${lcp.element.clientHeight}, size: ${lcp.element.clientWidth*lcp.element.clientHeight}</p>`);
        infoCont.insertAdjacentHTML('beforeend', `<p>LCP size: ${lcp.size}`);
        lcp.element.style.border = "2px solid red";
    }).observe({type: 'largest-contentful-paint', buffered: true});
});
</script>