---
#title: "How "
description: "How chrome browser calculates size of Text node elements."
prevUrl: "/pages/19-svg-text"
nextUrl: "/pages/candidates"
---
<div class="info-container">
    <div class="side-by-side">
        <div>
            <svg width="200" height="200" xmlns="http://www.w3.org/2000/svg">
              <image href="/1.jpg" height="200" width="200" />
            </svg>
        </div>
    </div>
    <button class="lets-check">Let's check</button>
</div>
<script>
setTimeout(function (){
    document.querySelector('.second-image').src = '/1.jpg';
}, 2000);
let infoCont = document.querySelector('.info-container');
document.querySelector('.lets-check').addEventListener('click', function(){
    new PerformanceObserver((entryList) => {
        let lcp = entryList.getEntries()[entryList.getEntries().length-1];
        infoCont.insertAdjacentHTML('beforeend', `<p>width: ${lcp.element.parentElement.clientWidth}, height: ${lcp.element.parentElement.clientHeight}, size: ${lcp.element.parentElement.clientWidth*lcp.element.parentElement.clientHeight}</p>`);
        infoCont.insertAdjacentHTML('beforeend', `<p>LCP size: ${lcp.size}`);
        lcp.element.parentElement.style.border = "2px solid red";
    }).observe({type: 'largest-contentful-paint', buffered: true});
});
</script>