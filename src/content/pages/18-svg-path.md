---
#title: "How "
description: "How chrome browser calculates size of Text node elements."
prevUrl: "/pages/17-two-images-one-changed"
nextUrl: "/pages/19-svg-text"
---
<div class="info-container">
    <div class="side-by-side">
        <div><svg width="300" class="common-header__svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 178 33"><path fill="#e4168d" class="common-header__svg-path" d="M141.4 10.8a2.9 2.9 0 0 0-3-2.9 3 3 0 0 0-3 2.9v11.4a3 3 0 0 0 3 2.9 2.9 2.9 0 0 0 3-2.9ZM157 15.8a5.1 5.1 0 0 0 2.6-2.9c.7-2.2.2-6.4.2-6.4s-.1-2.1-2-2.1-2 2.3-2 2.3a7.9 7.9 0 0 1-.3 2c-.3 1.1-.4 2.3-2.1 2.3s-3.1-.5-3.1-2.4v-5a3.5 3.5 0 0 0-3.6-3.6c-3.4 0-3.5 1.8-3.5 2.8v19.6c0 1 .4 2.7 2.9 2.7a2.5 2.5 0 0 0 2.8-2.8v-1c0-1 .3-2.9 1.9-2.9s1.8 2.2 1.8 2.7v1.5c0 1.2 1.5 2.5 3.4 2.5a3.7 3.7 0 0 0 3.9-4c-.1-3-2.9-5.3-2.9-5.3ZM177.4 14.4a1.5 1.5 0 0 0-1.4-1.5 1.5 1.5 0 0 0-1.5 1.5v9.2a1.5 1.5 0 0 0 1.5 1.5 1.5 1.5 0 0 0 1.4-1.5Z"></path><circle fill="#e4168d" class="common-header__svg-path" cx="162.1" cy="23.3" r="1.8"></circle><path fill="#e4168d" class="common-header__svg-path" d="M126.2 7.9c-4 0-4.2 1.9-4.2 1.9s0-1.9-2.5-1.9a2.4 2.4 0 0 0-2.5 2.3V22a3.2 3.2 0 0 0 3.5 3.1c2.5 0 3.6-1.4 3.6-3.1v-4.2c0-.6.1-2.4 2.7-2.4s2.9 1.6 2.9 2.4v5.3a2 2 0 0 0 2.2 2 1.9 1.9 0 0 0 2.1-1.7V17c0-3.5.9-9.1-7.8-9.1ZM71.1 7.9c-2.6 0-3.6.7-4 1.3s-2.9-1.3-5.7-1.3-4.2 1.9-4.2 1.9 0-1.9-2.5-1.9a2.5 2.5 0 0 0-2.5 2.3V22a3.3 3.3 0 0 0 3.6 3.1c2.4 0 3.6-1.4 3.6-3.1v-4.2c0-.6.1-2.4 2.6-2.4s2.9 1.6 2.9 2.4v5.3a2 2 0 0 0 2.2 2 1.9 1.9 0 0 0 2.1-1.7v-5.8c0-.7.2-2.2 2.5-2.2s2.9 1.6 2.9 2.4v5.3a2 2 0 0 0 2.2 2 1.9 1.9 0 0 0 2.1-1.7V17c.1-3.5 1-9.1-7.8-9.1ZM34.7 0a16.5 16.5 0 1 0 16.5 16.5A16.5 16.5 0 0 0 34.7 0ZM24 10.4a2.5 2.5 0 1 1 5 0 2.5 2.5 0 0 1-5 0Zm10.7 12.7a6.6 6.6 0 1 1 6.6-6.6 6.6 6.6 0 0 1-6.6 6.6Z"></path><path fill="#e4168d" class="common-header__svg-path" d="M8.7 7.9a9.1 9.1 0 0 0-3.7.8 2.8 2.8 0 0 0-2-.8 2.9 2.9 0 0 0-3 2.9V30a3.1 3.1 0 0 0 3 3 2.9 2.9 0 0 0 3-3v-5.3a12.6 12.6 0 0 0 2.8.4 8.6 8.6 0 1 0-.1-17.2Zm0 11.7a3.1 3.1 0 1 1 3.1-3.1 3.1 3.1 0 0 1-3.1 3.1ZM89.2 7.9a9.5 9.5 0 0 0-3.7.8 2.8 2.8 0 0 0-2-.8 2.9 2.9 0 0 0-3 2.9V30a2.9 2.9 0 0 0 3 3 3 3 0 0 0 3-3v-5.3a11 11 0 0 0 2.7.4 8.6 8.6 0 1 0 0-17.2Zm0 11.7a3.1 3.1 0 1 1 3.1-3.1 3.1 3.1 0 0 1-3.1 3.1ZM107.3 7.9a8.6 8.6 0 1 0 8.7 8.6 8.6 8.6 0 0 0-8.7-8.6Zm0 11.7a3.1 3.1 0 1 1 3.1-3.1 3.1 3.1 0 0 1-3.1 3.1ZM169.3 16.5a4.1 4.1 0 0 0-1.9.4 1.1 1.1 0 0 0-.9-.4A1.5 1.5 0 0 0 165 18v9.6a1.5 1.5 0 0 0 1.5 1.4 1.4 1.4 0 0 0 1.4-1.4v-2.7l1.4.2a4.3 4.3 0 0 0 4.3-4.4 4.2 4.2 0 0 0-4.3-4.2Zm0 5.9a1.6 1.6 0 0 1-1.6-1.6 1.6 1.6 0 0 1 1.6-1.6 1.6 1.6 0 0 1 1.6 1.6 1.7 1.7 0 0 1-1.6 1.6ZM142 2.6l-2-.8-1.6-1.8-1.6 1.8-2 .8 1.4 1.8v2.5l2.2-.8 2.2.8V4.4l1.4-1.8z"></path></svg></div>
    </div>
    <button class="lets-check">Let's check</button>
</div>
<script>
setTimeout(function (){
    document.querySelector('.second-image').src = '/1.jpg';
}, 2000);
let infoCont = document.querySelector('.info-container');
document.querySelector('.lets-check').addEventListener('click', function(){
    new PerformanceObserver((entryList) => {
        let lcp = entryList.getEntries()[entryList.getEntries().length-1];
        infoCont.insertAdjacentHTML('beforeend', `<p>width: ${lcp.element.clientWidth}, height: ${lcp.element.clientHeight}, size: ${lcp.element.clientWidth*lcp.element.clientHeight}</p>`);
        infoCont.insertAdjacentHTML('beforeend', `<p>LCP size: ${lcp.size}`);
        lcp.element.style.border = "2px solid red";
    }).observe({type: 'largest-contentful-paint', buffered: true});
});
</script>