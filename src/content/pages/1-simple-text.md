---
#title: "How "
description: "How chrome browser calculates size of Text node elements."
prevUrl: "/pages/candidates"
nextUrl: "/pages/2-simple-text-overflow"
---

<div class="info-container">
    <div class="side-by-side">
        <div><p style="display: inline-block;line-height: 19px;">Your eyes can deceive you, don't trust them.</p></div>
    </div>
    <button class="lets-check">Let's check</button>
</div>
<script>
let infoCont = document.querySelector('.info-container');
document.querySelector('.lets-check').addEventListener('click', function(){
    new PerformanceObserver((entryList) => {
        let lcp = entryList.getEntries()[entryList.getEntries().length-1];
        infoCont.insertAdjacentHTML('beforeend', `<p>width: ${lcp.element.clientWidth}, height: ${lcp.element.clientHeight}, size: ${lcp.element.clientWidth*lcp.element.clientHeight}</p>`);
        infoCont.insertAdjacentHTML('beforeend', `<p>LCP size: ${lcp.size}`);
        infoCont.insertAdjacentHTML('beforeend', `<br/><br/><br/><img src="/calc-text.png"/><br><p>Source: https://www.w3.org/TR/largest-contentful-paint/</p>`);
        lcp.element.style.border = "2px solid red";
    }).observe({type: 'largest-contentful-paint', buffered: true});
});
</script>