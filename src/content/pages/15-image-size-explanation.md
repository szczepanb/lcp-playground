---
title: "Image size calculations"
description: "Image size calculation for LCP candidates"
prevUrl: "14-two-images-one-bigger"
nextUrl: "16-two-images-one-bigger-lazy-load"
---
<b>image_size = visual_size * min(display_size, natural_size) / display_size</b>

<ul>
    <li><b>image_size</b> is the size of the image, as considered by the LCP algorithm</li>
    <li><b>visual_size</b> is the screen size occupied by the image</li>
    <li><b>display_size</b> is the total image size even if is overflow viewport</li>
    <li><b>natural_size</b> is the image size from image directly</li>
</ul>

We have tag image 200px x 200px.

Two images with size:
<ol>
    <li>200px x 200px</li>
    <li>150px x 150px</li>
</ol>
<style>
    table td{
        padding: 0 10px;
        text-align: right;
    }
    table tr:nth-child(odd) td{
       background:#ccc;
    }
    table tr:nth-child(even) td{
        background:#c9c9f5;
    }
</style>
<table>
    <thead>
        <tr>
            <th></th>
            <th>First image</th>
            <th>Second image</th>
        </tr>    
    </thead>
    <tbody>
        <tr>
            <td>Image</td>
            <td><img src="/1.jpg" width="200" height="200"></td>
            <td><img src="/3.jpg" width="200" height="200"></td>
        </tr>
        <tr>
            <td>visual_size</td>
            <td>200 * 200 = 40 000</td>
            <td>200 * 200 = 40 000</td>
        </tr>
        <tr>
            <td>display_size</td>
            <td>200 * 200 = 40 000</td>
            <td>200 * 200 = 40 000</td>
        </tr>
        <tr>
            <td>natural_size</td>
            <td>200 * 200 = 40 000</td>
            <td>150 * 150 = 22 500</td>
        </tr>
        <tr>
            <td>image_size</td>
            <td>40 000 * 40 000 / 40 000 = 40 000</td>
            <td>40 000 * 22 500 / 40 000 = 22 500</td>
        </tr>
    </tbody>
</table>