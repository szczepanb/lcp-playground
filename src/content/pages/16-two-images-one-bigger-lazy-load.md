---
#title: "How "
description: "How chrome browser calculates size of Text node elements."
prevUrl: "/pages/15-image-size-explanation"
nextUrl: "/pages/17-two-images-one-changed"
---
<div class="info-container">
    <div class="side-by-side">
        <div><img src="/1.jpg" width="100" height="100"/></div>
    </div>
    <button class="lets-check">Let's check</button>
</div>
<script>
setTimeout(function(){
    document.querySelector('.side-by-side').insertAdjacentHTML('beforeend', '<div><img src="/2.png" width="200" height="200"/></div>')
}, 3000);
let infoCont = document.querySelector('.info-container');
document.querySelector('.lets-check').addEventListener('click', function(){
    new PerformanceObserver((entryList) => {
        let lcp = entryList.getEntries()[entryList.getEntries().length-1];
        infoCont.insertAdjacentHTML('beforeend', `<p>width: ${lcp.element.clientWidth}, height: ${lcp.element.clientHeight}, size: ${lcp.element.clientWidth*lcp.element.clientHeight}</p>`);
        infoCont.insertAdjacentHTML('beforeend', `<p>LCP size: ${lcp.size}`);
        lcp.element.style.border = "2px solid red";
    }).observe({type: 'largest-contentful-paint', buffered: true});
});
</script>