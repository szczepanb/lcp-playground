---
#title: "How "
description: "How chrome browser calculates size of Text node elements."
prevUrl: "/pages/candidates"
nextUrl: "/pages/25-background-multiple-image-full-viewport"
---
<div class="info-container">
    <div class="side-by-side" style="background-image: url('/fullhd.jpg')"></div>
    <button class="lets-check">Let's check</button>
</div>
<style>
.side-by-side{
    position: fixed;
    top: 0;
    left: 0;
    width: 100svw;
    height: 100svh;
    background-repeat: no-repeat;
    background-position: center top;
}
button, p{
    position: relative;
    z-index: 1;
}
</style>
<script>
let infoCont = document.querySelector('.info-container');
document.querySelector('.lets-check').addEventListener('click', function(){
    new PerformanceObserver((entryList) => {
        let lcp = entryList.getEntries()[entryList.getEntries().length-1];
        infoCont.insertAdjacentHTML('beforeend', `<p>width: ${lcp.element.clientWidth}, height: ${lcp.element.clientHeight}, size: ${lcp.element.clientWidth*lcp.element.clientHeight}</p>`);
        infoCont.insertAdjacentHTML('beforeend', `<p>LCP size: ${lcp.size}`);
        lcp.element.style.border = "2px solid red";
    }).observe({type: 'largest-contentful-paint', buffered: true});
});
</script>