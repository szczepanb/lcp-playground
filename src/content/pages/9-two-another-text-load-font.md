---
#title: "How "
description: "How chrome browser calculates size of Text node elements."
prevUrl: "/pages/8-two-another-text-change-bigger"
nextUrl: "/pages/candidates"
---
<div class="info-container">
    <div class="side-by-side">
        <span>When you have some text, how can you choose a typeface? Many people—professional designers included—go through an app’s font menu until we find one we like. But the aim of this module is to show that there are many considerations that can improve our type choices. By setting some useful constraints to aid our type selection, we can also develop a critical eye for analyzing type along the way. When you have some text, how can you choose a typeface? Many people—professional designers included—go through an app’s font menu until we find one we like. But the aim of this module is to show that there are many considerations that can improve our type choices. By setting some useful constraints to aid our type selection, we can also develop a critical eye for analyzing type along the way.</span>
        <span style="font-family: Righteous, Tahoma, sans-serif">When you have some text, how can you choose a typeface? Many people—professional designers included—go through an app’s font menu until we find one we like. But the aim of this module is to show that there are many considerations that can improve our type choices. By setting some useful constraints to aid our type selection, we can also develop a When you have some text, how can you choose a typeface? Many people—professional designers included—go through an app’s font menu until we find one we like. But the aim of this module is to show that there are many considerations that can improve our type choices. By setting some useful constraints to aid our type selection, we can also develop a critical eye for analyzing type along the way.</span>
    </div>
    <button class="lets-check">Let's check</button>
</div>
<style>
    .side-by-side {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .side-by-side > * {
        margin: 10px;
        text-align: center;
    }
</style>
<script>
let infoCont = document.querySelector('.info-container');
document.querySelector('.lets-check').addEventListener('click', function(){
    new PerformanceObserver((entryList) => {
        let lcp = entryList.getEntries()[entryList.getEntries().length-1];
        infoCont.insertAdjacentHTML('beforeend', `<p>width: ${lcp.element.clientWidth}, height: ${lcp.element.clientHeight}, size: ${lcp.element.clientWidth*lcp.element.clientHeight}</p>`);
        infoCont.insertAdjacentHTML('beforeend', `<p>LCP size: ${lcp.size}`);
        lcp.element.style.border = "2px solid red";
    }).observe({type: 'largest-contentful-paint', buffered: true});
});
setTimeout(function(){
    document.head.insertAdjacentHTML('beforeend', `<link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">`);
}, 2000);
</script>