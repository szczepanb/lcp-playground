---
#title: "How "
description: "How chrome browser calculates size of Text node elements."
prevUrl: "/pages/11-simple-image-png"
nextUrl: "/pages/13-simple-image-small"
---
<div class="info-container">
    <div class="side-by-side">
        <div><img src="/1.jpg" width="200" height="200"/></div>
    </div>
    <button class="lets-check">Let's check</button>
</div>
<style>
.side-by-side img{
    position: fixed;
    bottom: -150px;
    left: 0;
    right: 0;
    margin: auto;
}
</style>
<script>
let infoCont = document.querySelector('.info-container');
document.querySelector('.lets-check').addEventListener('click', function(){
    new PerformanceObserver((entryList) => {
        let lcp = entryList.getEntries()[entryList.getEntries().length-1];
        infoCont.insertAdjacentHTML('beforeend', `<p>width: ${lcp.element.clientWidth}, height: ${lcp.element.clientHeight}, size: ${lcp.element.clientWidth*lcp.element.clientHeight}</p>`);
        infoCont.insertAdjacentHTML('beforeend', `<p>LCP size: ${lcp.size}`);
        lcp.element.style.border = "2px solid red";
    }).observe({type: 'largest-contentful-paint', buffered: true});
});
</script>