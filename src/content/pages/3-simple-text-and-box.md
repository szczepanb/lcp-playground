---
#title: "How "
description: "How chrome browser calculates size of Text node elements."
prevUrl: "/pages/2-simple-text-overflow"
nextUrl: "/pages/4-two-same-text-inline-block"
---

<div class="info-container">
    <div class="side-by-side">
        <div><p>Your eyes can deceive you, don't trust them.</p></div>
        <div style="width: 100px;height: 100px;background: yellow;"></div>
    </div>
    <button class="lets-check">Let's check</button>
</div>
<style>    
</style>
<script>
let infoCont = document.querySelector('.info-container');
document.querySelector('.lets-check').addEventListener('click', function(){
    new PerformanceObserver((entryList) => {
        let lcp = entryList.getEntries()[entryList.getEntries().length-1];
        infoCont.insertAdjacentHTML('beforeend', `<p>width: ${lcp.element.clientWidth}, height: ${lcp.element.clientHeight}, size: ${lcp.element.clientWidth*lcp.element.clientHeight}</p>`);
        infoCont.insertAdjacentHTML('beforeend', `<p>LCP size: ${lcp.size}`);
        lcp.element.style.border = "2px solid red";
    }).observe({type: 'largest-contentful-paint', buffered: true});
});
</script>