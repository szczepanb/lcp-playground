---
title: "Tools"
description: "Tools to measure LCP."
prevUrl: "/"
nextUrl: "/"
---
<ol>
    <li>Kibana dashboard
        <img src="/lcp-dashboard.png" width="640">
    </li>
    <li><a target="_blank" href="https://chrome.google.com/webstore/detail/web-vitals/ahfhijdlegdabablpippeagghigmibma">Web Vitals Extension for Chrome</a></li>
</ol>

