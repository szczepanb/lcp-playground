---
title: "Candidates"
description: "Candidates html tags to become a LCP element."
prevUrl: "/pages/history"
nextUrl: "/pages/1-simple-text"
---
Types of elements considered as LCP
<ol>
    <li><a href="/pages/1-simple-text">Block-level element containing text-nodes or inline-level elements.</a></li>
    <li><a href="/pages/10-simple-image">&lt;img\&gt; tags</a></li>
    <li><a href="/pages/18-svg-path">&lt;image\&gt; element in &lt;svg&gt; tags</a></li>
    <li><a href="/pages/21-video-poster">&lt;video\&gt; element with a poster image</a></li>
    <li><a href="/pages/24-background-image-full-viewport">Element with background-image loaded via url(...)</a></li>
</ol>

Elements exluded from considered as LCP

<ol>
    <li>Elements with opacity 0</li>
    <li>Elements cover full viewport</li>
    <li>Placeholder images or other images with low entropy</li>
</ol>

