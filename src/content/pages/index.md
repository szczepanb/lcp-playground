---
title: "Almost all about Largest Contentful Paint"
description: "Start journey about LCP."
prevUrl: "disabled"
nextUrl: "/pages/history"
---

<ol>
    <li><a href="/pages/history">History</a>,</li>
    <li><a href="/pages/candidates">Candidates</a>,</li>
    <li><a href="/pages/tools">Tools</a>,</li>
    <li><a href="/pages/bonus">Bonus</a></li>
</ol>