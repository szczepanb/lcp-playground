---
#title: "How "
description: "How chrome browser calculates size of Text node elements."
prevUrl: "/pages/18-svg-path"
nextUrl: "/pages/20-svg-data-image"
---
<div class="info-container">
    <div class="side-by-side">
        <div>
            <svg height="30" width="350">
              <text x="0" y="15" fill="red">Your eyes can deceive you, don't trust them.</text>
            </svg>
        </div>
    </div>
    <button class="lets-check">Let's check</button>
</div>
<script>
setTimeout(function (){
    document.querySelector('.second-image').src = '/1.jpg';
}, 2000);
let infoCont = document.querySelector('.info-container');
document.querySelector('.lets-check').addEventListener('click', function(){
    new PerformanceObserver((entryList) => {
        let lcp = entryList.getEntries()[entryList.getEntries().length-1];
        infoCont.insertAdjacentHTML('beforeend', `<p>width: ${lcp.element.clientWidth}, height: ${lcp.element.clientHeight}, size: ${lcp.element.clientWidth*lcp.element.clientHeight}</p>`);
        infoCont.insertAdjacentHTML('beforeend', `<p>LCP size: ${lcp.size}`);
        lcp.element.parentElement.style.border = "2px solid red";
    }).observe({type: 'largest-contentful-paint', buffered: true});
});
</script>