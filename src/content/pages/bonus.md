---
title: "Element timing API"
description: "Tools to measure LCP."
prevUrl: "/"
nextUrl: "/"
---
<p>
    &lt;img src="image.jpg" <b>elementtiming="big-image"</b> /&gt;<br>
    &lt;p <b>elementtiming="text"</b> id="text-id"&gt;text here&lt;/p&gt;
</p>
<br/>
<p>
    const observer = new PerformanceObserver((list) => {<br>
        &nbsp;&nbsp;&nbsp;&nbsp;list.getEntries().forEach((entry) => {<br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;console.log(entry);<br>
        &nbsp;&nbsp;&nbsp;&nbsp;});<br>
    });<br>
    observer.observe({ type: "element", buffered: true });<br>
</p>
