---
#title: "How "
description: "How chrome browser calculates size of Text node elements."
prevUrl: "/pages/21-video-poster"
nextUrl: "/pages/23-video-autoplay"
---
<div class="info-container">
    <div class="side-by-side">
        <div>
            <video width="320" height="240" controls="">
               <source src="https://www.w3schools.com/tags/movie.mp4" type="video/mp4">
               <source src="https://www.w3schools.com/tags/movie.ogg" type="video/ogg">
               Your browser does not support the video tag.
            </video>
        </div>
    </div>
    <button class="lets-check">Let's check</button>
</div>
<script>
setTimeout(function (){
    document.querySelector('.second-image').src = '/1.jpg';
}, 2000);
let infoCont = document.querySelector('.info-container');
document.querySelector('.lets-check').addEventListener('click', function(){
    new PerformanceObserver((entryList) => {
        let lcp = entryList.getEntries()[entryList.getEntries().length-1];
        infoCont.insertAdjacentHTML('beforeend', `<p>width: ${lcp.element.parentElement.clientWidth}, height: ${lcp.element.parentElement.clientHeight}, size: ${lcp.element.parentElement.clientWidth*lcp.element.parentElement.clientHeight}</p>`);
        infoCont.insertAdjacentHTML('beforeend', `<p>LCP size: ${lcp.size}`);
        lcp.element.parentElement.style.border = "2px solid red";
    }).observe({type: 'largest-contentful-paint', buffered: true});
});
</script>